# GIT O1: Git repository basics

DevOps training. Lesson GIT O1. This repository contains default config 
file for Nginx Web Server.

## Initial system config:

 * Platform: ESXi 7.0 virtual machine 
 * Guest OS: Ubuntu Linux 20.04 (64-bit) 
 
## Authors

 * **Evgeny Mukhanov** - *Initial work*
 
